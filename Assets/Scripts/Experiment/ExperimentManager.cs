﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Valve.Newtonsoft.Json;
using Valve.VR;

public class ExperimentManager : MonoBehaviour
{
       
    [Header("Experiment components")]
    public StroopGenerator stroopGenerator;

    public LableController lableController;
    [Header("Block variables")] 
    public int numberOfBlocks;
    public int numberOfTasks;

    private int _currentTaskNumber;
    private int _currentBlockNumber;

    private float _startStimuliTime;

    private float _responseTime;
    
    public SteamVR_Action_Boolean answereYes;
    public SteamVR_Action_Boolean answereNo;
    public SteamVR_Action_Boolean Space;
    
    // To check if we are already in a block 
    private bool _isBlockRunning;
    
    // data to be written into the CSV file
    private List<List<string>> _data;
    
    // the headers that will be used for the result CSV file at the end
    private List<string> _csvHeaders;
    public GameObject canvasObject;
    
    
    private void Start()
    {
        //canvasObject.SetActive(true);
        _currentTaskNumber = 0;
        _currentBlockNumber = 0;
        _data = new List<List<string>>();
        _csvHeaders = new List<string>();
        _csvHeaders.Add("Block Number");
        _csvHeaders.Add("Task Number");
        _csvHeaders.Add("Shown Stroop Color Name");
        _csvHeaders.Add("Shown Stroop Text");
        _csvHeaders.Add("Was Compatible");
        _csvHeaders.Add("Answered Correctly");
        _csvHeaders.Add("Reaction Time");
        
        // At the beginning we are not running a block yet
        _isBlockRunning = false;
    }

    private void ParticipantAnswered(bool participantAnswer)
    {
        List<string> taskData = new List<string>();
        _responseTime = Time.realtimeSinceStartup;
        
        // add the task (trial) data to the list
        taskData.Add(_currentBlockNumber.ToString());
        taskData.Add(_currentTaskNumber.ToString());
        taskData.Add(stroopGenerator.GetCurrentStroopColorName());
        taskData.Add(stroopGenerator.GetCurrentStroopText());
        taskData.Add(stroopGenerator.IsComplatible().ToString());
        taskData.Add((stroopGenerator.IsComplatible() == participantAnswer).ToString());
        // reaction time is simply response time minus the time at which the stimulus was started
        taskData.Add((_responseTime - _startStimuliTime).ToString());
        // add task (trial) data to the list that will later be written into the csv file
        _data.Add(taskData);
        
        GetNextTask();
    }
    
    // Update is called once per frame
    private void Update()
    {
        // is the block running or not
        if (_isBlockRunning)
        {
            if (answereYes.GetStateDown(SteamVR_Input_Sources.Any))
            {
                ParticipantAnswered(true);
            }
            if (answereNo.GetStateDown(SteamVR_Input_Sources.Any))
            {
                ParticipantAnswered(false);
            }
            
            // Participant said yes ( color matches the text)
            if (Input.GetKeyDown(KeyCode.Y))
            {
                ParticipantAnswered(true);
            }
            
            // participant said no ( color doesn't match the text)
            else if(Input.GetKeyDown(KeyCode.N))
            {
                ParticipantAnswered(false);
            }
        }
        else
        {
            if (Space.GetStateDown(SteamVR_Input_Sources.Any))
            {
                StartNextBlock();
            }
            
            // if we are between two blocks we start next block when space is pressed
            if (Input.GetKeyDown(KeyCode.Space))
            {
                StartNextBlock();
            }
        }
    }

    private void GetNextTask()
    {
       // check if there are any tasks (trials) left in the block
        if (_currentTaskNumber < numberOfTasks)
        {
            // continue with the next task (trial)
            stroopGenerator.GetNextStroopColor();
            stroopGenerator.GetNextStroopText();
            //set stimulus onset time to the current runtime of the program
            _startStimuliTime = Time.realtimeSinceStartup;
            _currentTaskNumber++;
        }
        
        // if the block is finished
        else
        {
            // check if there are any blocks left
            if(_currentBlockNumber < numberOfBlocks)
                // prompt input from user
                ShowMessage(Color.white,"End of block, press Trigger to continue");
            else
            {
                // continue with next block
                StartNextBlock();
            }
            _isBlockRunning = false;
        }
    }

    private void StartNextBlock()
    {
       // if there is a trial block left, start it 
        if (_currentBlockNumber < numberOfBlocks)
        {
            
            ResetTaskNumber();
            _isBlockRunning = true;
            _currentBlockNumber++;
            GetNextTask();
        }
        
        // else end the experiment
        else
        {
            ShowMessage(Color.magenta,"End of experiment, you are a russian spy!");
            
            // write everything into the result CSV file
            List<string> csvLines = CSVTools.GenerateCSV(_data, _csvHeaders);
            foreach (string line in csvLines)
            {
                Debug.Log(line);
            }

            CSVTools.SaveCSV(csvLines, Application.dataPath + "/Data/" + GUID.Generate());
        }
    }

    private void ShowMessage(Color32 color, string msg)
    {
        lableController.lableColor = color;
        lableController.ShowText(msg);
    }
    
    private void ResetTaskNumber()
    {
        _currentTaskNumber = 0;
    }
}
