﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LableController : MonoBehaviour
{
    public TextMeshProUGUI lable;
    public Color32 lableColor;
    public GameObject cross;

    public void ShowText(string msg, bool showCross = true)
    {
        cross.SetActive(showCross);

        lable.text = msg;
        lable.color = lableColor;
    }
}