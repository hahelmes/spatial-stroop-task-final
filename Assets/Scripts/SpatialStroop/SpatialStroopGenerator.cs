﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

public class SpatialStroopGenerator : MonoBehaviour
{


    [Range(0, 100)] public float ChanceOfCompatibility;
    public GameObject Arrow;
    public GameObject LeftLocation;
    public GameObject RightLocation;
    public GameObject BottomLocation;
    public GameObject TopLocation;
    
    private Vector3 _arrowLocation;
    private Vector3 _arrowRotation;
    private Dictionary<Vector3, string> _rotationToString;
    private Dictionary<string, Vector3> _stringToRotation;
    
    private Dictionary<string, Vector3> _stringToLocation;
    private Dictionary<Vector3, string> _locationToString;

    private string[] _locations;
    private string[] _rotations;
    
    
    // Start is called before the first frame update
    void Start()
    {

        _locations = new string[]{"left", "right", "up", "down"};
        _rotations = new string[]{"left", "right", "up", "down"};
        
        // location and rotation of the arrow; zero for now
        _arrowLocation = Vector3.zero;
        _arrowRotation = Vector3.zero;
        
        
        // this creates a rotation dictionary (map) between rotation strings (left, up, etc.) and their vector representations in Unity
        _stringToRotation = new Dictionary<string, Vector3>();
        _stringToRotation.Add("left", new Vector3(0f,90f, 0f));
        _stringToRotation.Add("right", new Vector3(0f, 90f, 180f));
        _stringToRotation.Add("up", new Vector3(0f, 90f, 90f));
        _stringToRotation.Add("down", new Vector3(0f, 90f, -90f));
        
        // this creates the inverse dictionary of the one above, useful for logging etc.
        _rotationToString = _stringToRotation.ToDictionary((i) => i.Value, (i) => i.Key);
        
    }

    public void CreateLocationDictionary(Transform fixationCross)
    {
        LeftLocation.transform.position = new Vector3(fixationCross.position.x, fixationCross.position.y, fixationCross.position.z  - 1);
        RightLocation.transform.position = new Vector3(fixationCross.position.x , fixationCross.position.y, fixationCross.position.z + 1);
        TopLocation.transform.position = new Vector3(fixationCross.position.x, fixationCross.position.y + 1, fixationCross.position.z);
        BottomLocation.transform.position = new Vector3(fixationCross.position.x, fixationCross.position.y - 1, fixationCross.position.z);
        
        // this creates a location dictionary (map) between location strings (left, bottom, etc.) and their vector representations in Unity
        _stringToLocation = new Dictionary<string, Vector3>();
        _stringToLocation.Add("left", LeftLocation.transform.localPosition);
        _stringToLocation.Add("right", RightLocation.transform.localPosition);
        _stringToLocation.Add("down", BottomLocation.transform.localPosition);
        _stringToLocation.Add("up", TopLocation.transform.localPosition);
        
        // this creates the inverse dictionary of the one above, again for logging purposes
        _locationToString  = _stringToLocation.ToDictionary((i) => i.Value, (i) => i.Key);
    }

    public void GetNextArrow()
    {
        
        // retrieve arrow location and arrow rotation for the next trial
        _arrowLocation = GetNextArrowLocation();
        _arrowRotation = GetNextArrowRotation();
        
        // set arrow location/rotation to new values
        Arrow.transform.localPosition = _arrowLocation;
        Arrow.transform.localEulerAngles = _arrowRotation;

    }

    Vector3 GetNextArrowLocation()
    {
        int locationIndex = Mathf.FloorToInt(Random.Range(0, _locations.Length));
        return _stringToLocation[_locations[locationIndex]];
    }

    Vector3 GetNextArrowRotation()
    {
        string _aLoc = GetArrowLocation();
        string _aRot;
        if (Random.Range(0, 100) < ChanceOfCompatibility)
        {
            _aRot = _aLoc;
            
        }
        else
        {
            do
            {
                int rotationIndex = Mathf.FloorToInt(Random.Range(0, _rotations.Length));
                _aRot = _rotations[rotationIndex];

            } while (_aLoc == _aRot);
        }
        return _stringToRotation[_aRot];
    }

    public string GetArrowLocation()
    {
        // look up location in dict and return it
        
        return _locationToString[_arrowLocation];
    }

    public string GetArrowRotation()
    {
        // look up rotation in dict and return it
        return _rotationToString[_arrowRotation];
    }

    public bool IsCompatible()
    {
        return GetArrowLocation() == GetArrowRotation();
    }
}
